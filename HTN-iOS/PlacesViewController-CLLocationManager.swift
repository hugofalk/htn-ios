//
//  PlacesViewController-CLLocationManager.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import CoreLocation
import MapKit

extension PlacesViewController: CLLocationManagerDelegate{
    @objc func centerMapOnLocation(_ location: CLLocation?) {
        
        guard let coordinate = location?.coordinate else {
            return
        }
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @objc func centerMapOnUserButtonClicked() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                mapView.setUserTrackingMode(mapView.userTrackingMode == MKUserTrackingMode.follow ? MKUserTrackingMode.none : MKUserTrackingMode.follow, animated: true)
                if mapView.userTrackingMode == MKUserTrackingMode.none{
                    mapView.showsUserLocation = false
                }
                return
            }
        }
        
        centerMapOnLocation(defaultLocation)
    }
    
    func setupUserLocation(){
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        //self.locationManager?.requestWhenInUseAuthorization()
        self.locationManager?.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.centerMapOnLocation(locations.last ?? defaultLocation)
        self.locationManager?.stopUpdatingLocation()
        
    }
}
