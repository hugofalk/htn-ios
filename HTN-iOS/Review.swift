//
//  Review.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import Foundation

struct Review: Codable{
    var createdAt: String
    var updatedAt: String
    var id: String
    
    var owner: String
    
    var stars: String//Int
    var text: String
    var user: String?
}

