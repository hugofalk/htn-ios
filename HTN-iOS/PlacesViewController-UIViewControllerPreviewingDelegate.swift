//
//  PlacesViewController-UIViewControllerPreviewingDelegate.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit

extension PlacesViewController: UIViewControllerPreviewingDelegate{
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = tableView.indexPathForRow(at: location) else {
            return nil
        }
        
        let detailViewController = createPanoramaViewControllerForIndexPath(indexPath: indexPath)
        
        return detailViewController
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        (viewControllerToCommit as! PanoramaViewController).downButton.isHidden = false

        self.present(viewControllerToCommit, animated: true, completion: nil)
        
    }
    
    private func createPanoramaViewControllerForIndexPath(indexPath: IndexPath) -> PanoramaViewController? {
        
        let place = getPlacesInMap()[indexPath.row]
        
        if place.panoramaImages?.first?.path != nil{
        
        let panoramaViewController = PanoramaViewController.storyboardInstance()
        panoramaViewController?.place = place
        
        return panoramaViewController!
        }
        else{
            return nil
        }
    }
    
}
