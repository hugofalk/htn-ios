//
//  PlaceTableViewCell.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit
import AlamofireImage

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var view: UIView!
//
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var customImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        view.layer.cornerRadius = 8
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.05
        view.clipsToBounds = false
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        
        customImageView.layer.cornerRadius = 8
        customImageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        customImageView.clipsToBounds = true

    }
    
    func setStars(stars: Int){
        
        star1.alpha = stars >= 1 ? 1 : 0.4
        star2.alpha = stars >= 2 ? 1 : 0.4
        star3.alpha = stars >= 3 ? 1 : 0.4
        star4.alpha = stars >= 4 ? 1 : 0.4
        star5.alpha = stars >= 5 ? 1 : 0.4
    }
    
    override func prepareForReuse() {
        customImageView?.af_cancelImageRequest()
        imagePath = nil
        self.nameLabel.text = nil
        self.setStars(stars: 0)
    }

    var imagePath: String? {
        didSet{
            
            if imagePath != nil{

                customImageView?.af_setImage(withURL: URL(string: imagePath!)!)
                
            }
            else{
                customImageView?.image = nil
            }
        }
    }

}
