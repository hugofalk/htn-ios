//
//  Place.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//
import Foundation

struct Place: Codable{
    var createdAt: String
    var updatedAt: String
    var id: String
    
    var name: String
    var lat: String
    var lng: String
    var address: String
    var city: String?
    
    var stairs: Bool?
    var WC: Bool?
    var reachability: Bool?

    var reviews: [Review]?
    var images: [Image]?
    var panoramaImages: [PanoramaImage]?

}
