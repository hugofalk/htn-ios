//
//  PlacesViewController-setup.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit
import MapKit

extension PlacesViewController{
    
    func setup(){
        setupTableView()
        registerForPreviewing(with: self, sourceView: tableView)
        UIApplication.shared.statusBarStyle = .lightContent
        askForPermission()
        fetchData()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupHeaderView()

    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.contentInset = UIEdgeInsets(top: headerViewHeight + 20, left: 0, bottom: 10, right: 0)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }

    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        
        fetchData {
            refreshControl.endRefreshing()
        }
    }
    
    func setupHeaderView(){
        
        headerView.frame = CGRect(x: 0, y: customNavbarBackground.frame.height, width: self.view.bounds.width, height: headerViewHeight)
        headerView.clipsToBounds = false
        
        view.addSubview(headerView)
        self.view.bringSubview(toFront: headerView)
        
        setupMapView()
        setupMapInfoLabel()
        setupMapTrackingButton()
        
    }
    
    
    
    func setupMapView(){
        mapView.mapType = .mutedStandard
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.showsUserLocation = false
        mapView.setUserTrackingMode(.none, animated: false)
        mapView.clipsToBounds = true
        
        mapView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: headerViewHeight)
        mapViewContainerView.frame = mapView.frame
        mapViewContainerView.clipsToBounds = true
        headerView.addSubview(mapViewContainerView)
        mapViewContainerView.addSubview(mapView)
        
        centerMapOnLocation(defaultLocation)
        setupUserLocation()
        
        mapView.tintColor = UIColor(named: "blue")

    }
    
    func setupMapInfoLabel(){
        
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.sizeToFit()
        
        
        mapInfoView.backgroundColor = UIColor(named: "blue")!
        mapInfoView.layer.cornerRadius = 10
        
        mapInfoView.frame.size.width = label.frame.size.width + 15
        mapInfoView.frame.size.height = label.frame.size.height + 8
        mapInfoView.center.x = view.center.x
        mapInfoView.center.y = headerViewHeight
        
        mapInfoView.layer.shadowColor = UIColor.black.cgColor
        mapInfoView.layer.shadowOpacity = 0.2
        mapInfoView.layer.shadowOffset = CGSize.zero
        mapInfoView.layer.shadowRadius = 5
        mapInfoView.alpha = 0
        
        
        
        
        headerView.addSubview(mapInfoView)
        
        label.center = CGPoint(x:mapInfoView.frame.size.width/2, y:mapInfoView.frame.size.height/2)
        mapInfoView.addSubview(label)
        
    }
    
    func updateMapInfoLabel(_ str: String){
        
        let label = (mapInfoView.subviews.first as? UILabel)
        label?.text = str
        label?.sizeToFit()
        mapInfoView.frame.size.width = (label?.frame.size.width ?? 0) + 15
        mapInfoView.frame.size.height = (label?.frame.size.height ?? 0) + 8
        mapInfoView.center.x = view.center.x
        label?.center = CGPoint(x:mapInfoView.frame.size.width/2, y:mapInfoView.frame.size.height/2)
        
        if mapInfoView.alpha == 0{
            UIView.animate(withDuration: 1.2) {
                self.mapInfoView.alpha = 1
            }
        }
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = headerViewHeight - (scrollView.contentOffset.y + headerViewHeight + 20)
        let h = max(0, y)
        headerView.frame.size.height = h
        mapViewContainerView.frame.size.height = h
        mapInfoView.center.y = min(max(mapInfoView.frame.height,y), headerViewHeight)
    }
    
    func setupMapTrackingButton(){
        
        let buttonView = UIView()
        buttonView.frame = CGRect(x: view.bounds.width - (10 + (35)), y: 10, width: 35, height: 35)
        buttonView.layer.cornerRadius = 5
        buttonView.backgroundColor = UIColor.clear
        buttonView.layer.shadowColor = UIColor.black.cgColor
        buttonView.layer.shadowOpacity = 0.2
        buttonView.clipsToBounds = false
        buttonView.layer.shadowOffset = CGSize.zero
        buttonView.layer.shadowRadius = 5
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PlacesViewController.centerMapOnUserButtonClicked))
        buttonView.addGestureRecognizer(tapGesture)
        
        mapViewContainerView.addSubview(buttonView)
        
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        blurView.frame.size = buttonView.frame.size
        blurView.clipsToBounds = true
        blurView.layer.cornerRadius = 5
        buttonView.addSubview(blurView)
        
        let image = UIImage(named: "locationArrow")
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)
        
        let button = UIButton(type: UIButtonType.custom) as UIButton
        button.frame.size = CGSize(width: 20, height: 20)
        button.center = CGPoint(x: 35/2, y: 35/2)
        
        
        button.setImage(tintedImage, for: .normal)
        button.tintColor = UIColor(named: "blue")
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(PlacesViewController.centerMapOnUserButtonClicked), for: .touchUpInside)
        buttonView.addSubview(button)
        
        
    }

    func askForPermission(){
        
        let status  = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            
            let alert = UIAlertController(title: "Activate locations services", message: "We need your location to show nearby places", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            }))
            alert.addAction(UIAlertAction(title: "Activate", style: .default, handler: { (action) in
                self.locationManager?.requestWhenInUseAuthorization()
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
    }


    
}
