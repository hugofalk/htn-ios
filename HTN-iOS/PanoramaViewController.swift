//
//  PanoramaViewController.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit
import Alamofire
import CTPanoramaView

class PanoramaViewController: UIViewController {

    @IBOutlet weak var panoramaView: CTPanoramaView!
    @IBOutlet weak var downButton: UIButton!
    
    public var place : Place? {
        didSet{

            if let imagePath = place?.panoramaImages?.first?.path {
                
                Alamofire.request(imagePath, method: .get).responseImage { response in
                    
                    if let data = response.result.value {
                        self.panoramaView.controlMethod = CTPanoramaControlMethod.motion
                        
                        self.panoramaView.isHidden = false

                        self.panoramaView.image = data
                        
                        if self.place?.panoramaImages?.first?.type == "spherical"{
                            self.panoramaView.panoramaType = .spherical
                        }
                        else{
                            self.panoramaView.panoramaType = .cylindrical
                        }

                        
                    } else {
                        
                    }
                }
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
downButton.isHidden = true
        panoramaView.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pinch(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
//        UIView.animate(withDuration: 0.4, animations: {
//            self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//            self.view.alpha = 0
//
//        }) { (e) in
//            self.dismiss(animated: true, completion: nil)
//
//        }
    }

    public static func storyboardInstance() -> PanoramaViewController? {
        let storyboard = UIStoryboard(name:
            "Panorama", bundle: nil)
        return storyboard.instantiateInitialViewController() as?
        PanoramaViewController
    }

}
