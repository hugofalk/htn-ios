//
//  Image.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import Foundation

struct Image: Codable{
    var id: String
    var path: String?
}
