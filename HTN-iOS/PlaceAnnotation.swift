//
//  PlaceAnnotation.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import MapKit

class PlaceAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    let place: Place
    
    init(place: Place) {
        self.place = place
        
        self.coordinate = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(exactly: Double(place.lat)!)!, longitude: CLLocationDegrees(exactly: Double(place.lng)!)!)
        super.init()
    }
    
    var title: String? {
        return place.name
    }
}
