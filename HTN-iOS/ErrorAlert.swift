//
//  ErrorAlert.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit

class ErrorAlert: UIAlertController {
    
    convenience init(networkMessage: Bool = true, canIgnore: Bool = true, retry: (() -> ())? ) {
        let title = "Something went wrong"
        let message = networkMessage ? "Connect to a network and try again." : ""
        
        self.init(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        
        let action2 = UIAlertAction(title: "Retry", style: .default) { (action) in
            retry?()
        }
        
        canIgnore ? self.addAction(action) : nil
        retry != nil ? self.addAction(action2) : nil
        
        
    }
    
    
}
