//
//  PlaceViewController.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class PlacesViewController: UIViewController {
    var places = [Place]()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var typeTabBar: UITabBar!
    
    @IBOutlet weak var customNavbarBackground: UIView!
    @IBOutlet weak var customNavBarView: UIView!
    
    var locationManager : CLLocationManager?
    var locatonTimer : Timer?
    //Waterloo
    var defaultLocation: CLLocation = CLLocation(latitude: 43.472353 , longitude: -80.526340)
    
    //Gothenburg
//    var defaultLocation: CLLocation = CLLocation(latitude: 57.708870, longitude: 11.974560)

        let regionRadius: CLLocationDistance = 4000
        
    let mapView = MKMapView()
    let mapViewContainerView = UIView()
    let mapInfoView = UIView()
    let headerView = UIView()
    let headerViewHeight : CGFloat = 250

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func fetchData(_ completion: ( () -> Void)? = nil){
        guard let apiPath = ProcessInfo.processInfo.environment["apiPath"] else{
            return
        }
        
        Alamofire.request(apiPath + "place").responseJSON { response in
            
            if let data = response.data {
                do{
                    self.places = try JSONDecoder().decode([Place].self, from: data)
                }
                catch{
                    print(error)
                    self.present(ErrorAlert(networkMessage: true, canIgnore: false, retry: {
                        self.fetchData(completion)
                    }), animated: true, completion: nil)
                    
                }
                self.createMapAnnotations({
                    self.tableView.reloadData()
                })
                
                
            }
            else{
                self.present(ErrorAlert(networkMessage: true, canIgnore: false, retry: {
                    self.fetchData(completion)
                }), animated: true, completion: nil)
            }
            
            completion?()
            
        }
        
    }


}
