//
//  PlacesViewController-MapKit.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import MapKit

extension PlacesViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation{
            return nil
        }
        
        if annotation is MKClusterAnnotation {
            let markerAnnotationView = MKMarkerAnnotationView()
            markerAnnotationView.markerTintColor = UIColor(named: "Teal blue")
            markerAnnotationView.glyphText = "+"
            
            return markerAnnotationView
        }
        
        if annotation is PlaceAnnotation{
            
            let annotationIdentifier = "AnnotationIdentifier"
            var annotationView : MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKMarkerAnnotationView
            
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.animatesWhenAdded = false
            
            annotationView?.markerTintColor = UIColor(named: "blue")
            
            let reviews = (annotation as! PlaceAnnotation).place.reviews
            if reviews?.count ?? 0 > 0{
                var sum = 0
                reviews?.forEach({ (review) in
                    sum += Int(review.stars)!
                })
                let average = (sum)/(reviews!.count )
                annotationView?.glyphText = "\(Int(average))"
            }
            else{
                annotationView?.glyphText = "-"
            }
            
            
            annotationView?.displayPriority = .required
            annotationView?.titleVisibility = .visible
            annotationView?.clusteringIdentifier = "AnnotationClusterIdentifier"
            
            return annotationView
        }
            
            
        else{
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if view.annotation is MKClusterAnnotation{
            
            let coordinateRegion =  MKCoordinateRegionMakeWithDistance((view.annotation?.coordinate)!, mapView.currentRadius() * 0.5, mapView.currentRadius() * 0.5)
            mapView.setRegion(coordinateRegion, animated: true)
            mapView.deselectAnnotation(view.annotation, animated: true)
        }
        
        if view.annotation is PlaceAnnotation{
            
            mapView.deselectAnnotation(view.annotation, animated: true)
            let id = (view.annotation as? PlaceAnnotation)?.place.id
            
            if id != nil{
                
                var index : Int?
                for i in 0..<getPlacesInMap().count{
                    if getPlacesInMap()[i].id == id{
                        index = i
                    }
                }
                if index == nil {
                    return
                }
                
                tableView.scrollToRow(at: IndexPath(row: index!, section: 0), at: .middle, animated: true)
                
                let cell = tableView.cellForRow(at: IndexPath(row: index!, section: 0)) as? PlaceTableViewCell
                
                
                if cell != nil{
                    
                    
                    UIView.animate(withDuration: 0.1,
                                   delay: 0,
                                   usingSpringWithDamping: 300.0,
                                   initialSpringVelocity: 5.0,
                                   options: [.allowUserInteraction, .beginFromCurrentState],
                                   animations: {
                                    cell?.view.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                                    
                    }, completion: { (success) in
                        UIView.animate(withDuration: 0.5,
                                       delay: 0.06,
                                       usingSpringWithDamping: 300.0,
                                       initialSpringVelocity: 5.0,
                                       options: [.allowUserInteraction, .beginFromCurrentState],
                                       animations: {
                                        cell?.view.transform = CGAffineTransform.identity
                                        
                        })
                        
                    })
                    
                }
            }
            
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
        }
        
        
    }
    

    func getPlacesInMap() -> [Place]{

        var tmpPlaces = [Place]()
        mapView.visibleAnnotations().forEach { (annotaion) in
            if annotaion is PlaceAnnotation{
                tmpPlaces.append((annotaion as! PlaceAnnotation).place)

            }
        }
        return tmpPlaces
        
    }
    
    func createMapAnnotations(_ closure: (() -> Void)? = nil){
        
        var annotations = [MKAnnotation]()
        places.forEach { (place) in
            annotations.append(PlaceAnnotation(place: place))
        }
        
        DispatchQueue.main.async {
            
            self.mapView.removeAnnotations(self.mapView.annotations )
            self.mapView.addAnnotations(annotations)
            self.mapView.setNeedsDisplay()
            closure?()
        }
        
    }


}

