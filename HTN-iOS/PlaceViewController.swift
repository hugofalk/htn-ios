//
//  PlaceViewController.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit

class PlaceViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var stairs: UIImageView!
    @IBOutlet weak var wc: UIImageView!
    @IBOutlet weak var reachability: UIImageView!
    
    @IBOutlet weak var stairsLabel: UILabel!
    @IBOutlet weak var wcLabel: UILabel!
    @IBOutlet weak var reachabilityLabel: UILabel!
    
    
    @IBOutlet weak var insetConstraint: NSLayoutConstraint!
    public var place : Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        addressLabel.text = place?.address
        nameLabel.text = place?.name
        scrollView.contentInsetAdjustmentBehavior = .never
        
        guard ProcessInfo.processInfo.environment["apiPath"] != nil else{
            return
        }
        
        if let imagePath = place?.images?.first?.path {
            imageView?.af_setImage(withURL: URL(string: imagePath)!)
            
        }
        else{
            insetConstraint.constant = 10
        }
        
        let reviews = place?.reviews
        if reviews?.count ?? 0 > 0{
            var sum = 0
            reviews!.forEach({ (review) in
                sum += Int(review.stars)!
            })
            let average = (sum)/(reviews!.count )
            setStars(stars: Int(average))
        }
        else{
            setStars(stars: Int(0))
        }

        let image = UIImage(named: "stairs")
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)
        stairs.image = tintedImage
        
        let image2 = UIImage(named: "wc")
        let tintedImage2 = image2?.withRenderingMode(.alwaysTemplate)
        wc.image = tintedImage2

        let image3 = UIImage(named: "reachability")
        let tintedImage3 = image3?.withRenderingMode(.alwaysTemplate)
        reachability.image = tintedImage3
        
        
        if place?.stairs ?? false{
            stairs.tintColor = UIColor(named: "blue")
            stairsLabel.textColor = UIColor(named: "blue")

        }
        else{
            stairs.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            stairsLabel.textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)

        }
        
        
        if place?.reachability ?? false{
            reachability.tintColor = UIColor(named: "blue")
            reachabilityLabel.textColor = UIColor(named: "blue")
            
        }
        else{
            reachability.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            reachabilityLabel.textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            
        }

        
        if place?.WC ?? false{
            wc.tintColor = UIColor(named: "blue")
            wcLabel.textColor = UIColor(named: "blue")
            
        }
        else{
            wc.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            wcLabel.textColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
            
        }

        
    }
    
    func setStars(stars: Int){
        star1.alpha = stars >= 1 ? 1 : 0.4
        star2.alpha = stars >= 2 ? 1 : 0.4
        star3.alpha = stars >= 3 ? 1 : 0.4
        star4.alpha = stars >= 4 ? 1 : 0.4
        star5.alpha = stars >= 5 ? 1 : 0.4
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func down(_ sender: Any) {
    dismiss(animated: true)
    }
    
    public static func storyboardInstance() -> PlaceViewController? {
        let storyboard = UIStoryboard(name:
            "Place", bundle: nil)
        return storyboard.instantiateInitialViewController() as?
        PlaceViewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!)  {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.commonInit()
    }
    
    func commonInit() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
    }

    
}
