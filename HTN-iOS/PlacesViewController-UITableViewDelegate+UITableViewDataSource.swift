//
//  PlacesViewController-tableview.swift
//  HTN-iOS
//
//  Created by Hugo Falk on 2018-09-15.
//  Copyright © 2018 Hugo Falk. All rights reserved.
//

import UIKit

extension PlacesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceTableViewCell") as! PlaceTableViewCell
        cell.nameLabel.text = getPlacesInMap()[indexPath.row].name
        
        let reviews = getPlacesInMap()[indexPath.row].reviews
        if reviews?.count ?? 0 > 0{
            var sum = 0
            reviews!.forEach({ (review) in
                sum += Int(review.stars)!
            })
            let average = (sum)/(reviews!.count )
            cell.setStars(stars: Int(average))
        }
        else{
            cell.setStars(stars: Int(0))
        }
        
        if getPlacesInMap()[indexPath.row].images != nil && getPlacesInMap()[indexPath.row].images!.count > 0 {
            cell.imagePath = getPlacesInMap()[indexPath.row].images!.first?.path
        }
        else{
            cell.imagePath = nil
        }
        
        return cell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.present(createPlaceViewControllerForIndexPath(indexPath: indexPath)!, animated: true, completion: nil)
        
    }
    
    private func createPlaceViewControllerForIndexPath(indexPath: IndexPath) -> PlaceViewController? {
        
        let place = getPlacesInMap()[indexPath.row]

        let placeViewController = PlaceViewController.storyboardInstance()
        placeViewController?.place = place
        
        return placeViewController!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        updateMapInfoLabel("\(getPlacesInMap().count != 0 ? "\(getPlacesInMap().count)" : "no") place\((getPlacesInMap().count > 1 || getPlacesInMap().count == 0) ? "s" : "")")
        return getPlacesInMap().count
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PlaceTableViewCell
        
        
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       usingSpringWithDamping: 300.0,
                       initialSpringVelocity: 5.0,
                       options: [.allowUserInteraction, .beginFromCurrentState], //[.Alert, .Badge]
            animations: {
                cell.view.transform = CGAffineTransform(scaleX: 0.96, y: 0.96)
                cell.alpha = 0.92
                
        })
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PlaceTableViewCell
        UIView.animate(withDuration: 0.3,
                       delay: 0.06,
                       usingSpringWithDamping: 300.0,
                       initialSpringVelocity: 5.0,
                       options: [.allowUserInteraction, .beginFromCurrentState], //[.Alert, .Badge]
            animations: {
                cell.view.transform = CGAffineTransform.identity
                cell.alpha = 1.0
                
                
        })
    }
    
}
